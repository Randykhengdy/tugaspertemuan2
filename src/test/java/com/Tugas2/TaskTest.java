package com.Tugas2;

import org.junit.Test;

import static org.junit.Assert.*;

public class TaskTest {
    @Test
    public void testAddTaskList() {
        String expectedTodo = "1. Do Dishes activity [NOT DONE]";

        Task task = new Task(1, "Do Dishes", "activity");

        assertEquals(expectedTodo, task.getTask());
    }

    @Test
    public void testTaskDataIsNull() {
        Task task = new Task();

        assertNull(task.getTask());
    }

    @Test
    public void testTaskStatusIsDoneOrNotDone() {
        Task task = new Task(1, "Do Dishes", "activity");

        boolean actual = task.CheckTaskStatus();

        assertTrue(actual);
    }
}