package com.Tugas2;

import org.junit.Test;

import static org.junit.Assert.*;

public class ToDoListTest {
    @Test
    public void testAddToList() {
        String expectedTodo = "1. Do Dishes activity [NOT DONE]\n2. Learn Java activity [NOT DONE]\n3. Learn TDD activity [NOT DONE]";
        ToDoList toDoList = new ToDoList();

        toDoList.addToDoList(1, "Do Dishes", "activity");
        toDoList.addToDoList(2, "Learn Java", "activity");
        toDoList.addToDoList(3, "Learn TDD", "activity");
        String actual = toDoList.getToDoList();

        assertEquals(expectedTodo, actual);
    }

    @Test
    public void testGetToDoListDataOnNullReferences() {
        String actual = "Task does not yet exist";

        ToDoList toDoList = new ToDoList();

        assertEquals("Task does not yet exist", toDoList.getToDoList());
    }

    @Test
    public void testChangeStatus() {
        String expectedTodo = "1. Do Dishes activity [DONE]\n2. Learn Java activity [DONE]\n3. Learn TDD activity [NOT DONE]";

        ToDoList toDoList = new ToDoList();

        toDoList.addToDoList(1, "Do Dishes", "activity");
        toDoList.addToDoList(2, "Learn Java", "activity");
        toDoList.addToDoList(3, "Learn TDD", "activity");
        toDoList.updateStatusToDone(1);
        toDoList.updateStatusToDone(2);

        String actual = toDoList.getToDoList();

        assertEquals(expectedTodo, actual);
    }

    @Test
    public void testContainTaskId() {

        ToDoList toDoList = new ToDoList();

        toDoList.addToDoList(1, "Do Dishes", "activity");
        toDoList.addToDoList(2, "Learn Java", "activity");
        toDoList.addToDoList(3, "Learn TDD", "activity");


        assertTrue(toDoList.isContainTaskId(3));

    }
    @Test
    public void testShowByCategory() {

        String expectedToDo = "1. Do Dishes activity [NOT DONE]";
        ToDoList toDoList = new ToDoList();

        toDoList.addToDoList(1, "Do Dishes", "activity");
        toDoList.addToDoList(2, "Learn Java", "study");
        toDoList.addToDoList(3, "Learn TDD", "study");

        String actual = toDoList.showByCategory("activity");
        assertEquals(expectedToDo,actual);

    }


}