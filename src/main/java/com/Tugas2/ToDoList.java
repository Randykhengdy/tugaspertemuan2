package com.Tugas2;


import java.util.ArrayList;
import java.util.Scanner;

public class ToDoList {

    private ArrayList<Task> toDoList = new ArrayList<>();

    Scanner scanner = new Scanner(System.in);

    public void addMenu() {
        int tempId;
        String tempName;
        String tempCategory;


        do {
            System.out.println(getToDoList());
            System.out.println("Input Task Id");
            tempId = scanner.nextInt();
            scanner.nextLine();
            if (isContainTaskId(tempId)) {
                System.out.println("Id already exists");
            }

        } while (isContainTaskId(tempId));

        System.out.println("Input Task Name");
        tempName = scanner.nextLine();
        System.out.println("Input Task Category");
        tempCategory = scanner.nextLine();

        addToDoList(tempId, tempName, tempCategory);

    }

    public void updateMenu() {
        int idTaskToBeUpdate;
        do {
            System.out.println(getToDoList());
            System.out.println("Input Task Id that you want change to done");
            idTaskToBeUpdate = scanner.nextInt();
            scanner.nextLine();
            if (isContainTaskId(idTaskToBeUpdate) == false) {
                System.out.println("Id does not exist");
            } else {
                updateStatusToDone(idTaskToBeUpdate);
            }

        } while (isContainTaskId(idTaskToBeUpdate) == false);

    }

    public void deleteMenu() {
        int idTaskToBeDelete;
        do {
            System.out.println(getToDoList());
            System.out.println("Input Task Id that you want to delete");
            idTaskToBeDelete = scanner.nextInt();
            scanner.nextLine();
            if (isContainTaskId(idTaskToBeDelete) == false) {
                System.out.println("Id does not exist");
            } else {
                deleteStatusById(idTaskToBeDelete);
                break;
            }

        } while (isContainTaskId(idTaskToBeDelete) == false);

    }

    public void viewByCategoryMenu() {
        String categoryToView;
        System.out.println(showAllCategory().toString());
        System.out.println("Input category you want to view");
        categoryToView = scanner.nextLine();

        System.out.println(showByCategory(categoryToView));
        System.out.println("Press enter to continue");
        scanner.nextLine();
    }

    public String showByCategory(String categoryToView) {
        StringBuilder task = new StringBuilder();
        int totalId = 0;
        for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {


            if (toDoList.get(indexOfList).getCategory().equalsIgnoreCase(categoryToView)) {
                task.append(toDoList.get(indexOfList).getTask()).append("\n");
            }
        }
        task.delete(task.length() - 1, task.length());
        return task.toString();
    }

    private ArrayList showAllCategory() {
        ArrayList<String> Category = new ArrayList<>();

        for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {
            if (Category.contains(toDoList.get(indexOfList).getCategory()) == false) {
                Category.add(toDoList.get(indexOfList).getCategory());
            }
        }
        return Category;
    }

    public boolean isContainTaskId(int id) {
        for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {
            if (toDoList.get(indexOfList).getId() == id) {
                return true;
            }
        }
        return false;
    }

    public void addToDoList(int id, String name, String category) {

        Task task = new Task(id, name, category);

        toDoList.add(task);
    }

    public String getToDoList() {
        StringBuilder task = new StringBuilder();

        if (toDoList.size() == 0) {
            return "Task does not yet exist";
        } else {
            for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {
                task.append(toDoList.get(indexOfList).getTask()).append("\n");
            }
            task.delete(task.length() - 1, task.length());
            return task.toString();
        }
    }

    void updateStatusToDone(int idOfTask) {
        for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {
            if (toDoList.get(indexOfList).getId() == idOfTask) {
                Task taskToBeChange = toDoList.get(indexOfList);
                taskToBeChange.setStatus("DONE");
                toDoList.set(indexOfList, taskToBeChange);
            }
        }
    }

    void deleteStatusById(int idOfTask) {
        for (int indexOfList = 0; indexOfList < toDoList.size(); indexOfList++) {
            if (toDoList.get(indexOfList).getId() == idOfTask) {
                toDoList.remove(indexOfList);
            }
        }

    }


}
