package com.Tugas2;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int userInput = 0;

        View view = new View();

        Scanner scanner = new Scanner(System.in);

        ToDoList toDoList = new ToDoList();
        toDoList.addToDoList(1, "Randy1", "Aktivitas");
        toDoList.addToDoList(2, "Randy2", "Main");
        toDoList.addToDoList(3, "Randy3", "Belajar");
        do {

            System.out.println(toDoList.getToDoList());

            view.menu();

            userInput = scanner.nextInt();
            scanner.nextLine();

            switch (userInput) {
                case 1: {
                    toDoList.addMenu();
                    break;
                }
                case 2: {
                    toDoList.updateMenu();
                    break;
                }
                case 3: {
                    toDoList.deleteMenu();
                    break;
                }
                case 4: {
                    toDoList.viewByCategoryMenu();
                    break;
                }
                default:
            }
        } while (userInput != 5);

    }


}
