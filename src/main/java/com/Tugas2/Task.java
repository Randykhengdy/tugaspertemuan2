package com.Tugas2;

import java.util.Objects;

public class Task {
    private String name;
    private String status;
    private String category;
    private int id;

    public Task() {

    }

    public Task(int id, String name, String category) {
        this.name = name;
        this.id = id;
        this.category = category;
        this.status = "NOT DONE";
    }

    public String getTask() {
        if (name == null) {
            return null;
        } else {
            return id + ". " + this.name + " " + this.category + " [" + this.status + "]";
        }
    }

    public Boolean CheckTaskStatus() {
        return status.equals("DONE") || status.equals("NOT DONE");
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCategory() {
        return category;
    }

    public int getId() {
        return id;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                Objects.equals(name, task.name) &&
                Objects.equals(status, task.status);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, status, id);
    }

    @Override
    public String toString() {
        return "Task{" +
                "name='" + name + '\'' +
                ", status='" + status + '\'' +
                ", id=" + id +
                '}';
    }


}



